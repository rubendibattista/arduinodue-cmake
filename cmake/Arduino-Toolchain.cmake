set(BOARD_VARIANT arduino_due_x)

#######################################################################
#                     Find Toolchain executables                      #
#######################################################################
# Find ASM compiler
find_program(CMAKE_ASM_COMPILER arm-none-eabi-gcc REQUIRED)

# Find C compiler
find_program(CMAKE_C_COMPILER arm-none-eabi-gcc REQUIRED)

# Find C++ compiler
find_program(CMAKE_CXX_COMPILER arm-none-eabi-g++ REQUIRED)

# Find AR required for linkage
find_program(CMAKE_AR arm-none-eabi-ar REQUIRED)

# Find Ranlib required for linkage
find_program(CMAKE_RANLIB arm-none-eabi-ranlib REQUIRED)

# Find NM
find_program(CMAKE_NM arm-none-eabi-nm REQUIRED)

# Find objcopy
find_program(CMAKE_OBJCOPY arm-none-eabi-objcopy REQUIRED)


#######################################################################
#                       Toolchain Configuration                       #
#######################################################################
set(CMAKE_SYSTEM_NAME Generic)
set(CMAKE_SYSTEM_PROCESSOR arm)
# set(CMAKE_SYSROOT ${TOOLCHAIN_SDK_SYSROOT})


set(CMAKE_FIND_ROOT_PATH_MODE_PROGRAM NEVER)
set(CMAKE_FIND_ROOT_PATH_MODE_LIBRARY ONLY)
set(CMAKE_FIND_ROOT_PATH_MODE_INCLUDE ONLY)
set(CMAKE_FIND_ROOT_PATH_MODE_PACKAGE ONLY)

set(TOOLCHAIN_ARCH sam3xa)


#######################################################################
#                    Compiler Flags and definitions                   #
#######################################################################
set(CMAKE_EXE_LINKER_FLAGS "-specs=nosys.specs -specs=nano.specs")

# Flags specific to build the core SDK. i.e. they're not propagated to
# sketch code

list(APPEND TOOLCHAIN_SDK_DEFINITIONS
    printf=iprintf
    F_CPU=84000000 
    ARDUINO=10611
    __SAM3X8E__
    USB_PID=0x003e
    USB_VID=0x2341
    USBCON
    ARDUINO_SAM_DUE 
    ARDUINO_ARCH_SAM
    USB_MANUFACTURER="Arduino LLC"
    USB_PRODUCT="Arduino Due"
)

add_compile_definitions(${TOOLCHAIN_SDK_DEFINITIONS})

string(APPEND TOOLCHAIN_SDK_COMMON_FLAGS
" -g"
" -Os"
" -w"
" -ffunction-sections"
" -fdata-sections"
" --param max-inline-insns-single=500"
" -mcpu=cortex-m3"
" -mthumb"
)

string(APPEND TOOLCHAIN_SDK_CXX_FLAGS
    ${TOOLCHAIN_SDK_COMMON_FLAGS}
    " -fno-rtti"
    " -fno-exceptions"
    " -std=gnu++11"
    " -Wall"
    " -Wextra"
)

set(CMAKE_CXX_FLAGS ${TOOLCHAIN_SDK_CXX_FLAGS} CACHE INTERNAL "")

string(APPEND TOOLCHAIN_SDK_C_FLAGS
    ${TOOLCHAIN_SDK_COMMON_FLAGS}
    " -std=gnu11"
)

set(CMAKE_C_FLAGS ${TOOLCHAIN_SDK_C_FLAGS} CACHE INTERNAL "")
